USE [BD_CONTRATO]
GO
SET IDENTITY_INSERT [dbo].[Acesso] ON 

INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (1, N'CriarEditar', 1)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (2, N'Excluir', 1)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (3, N'Consultar', 1)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (4, N'CriarEditar', 2)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (5, N'Excluir', 2)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (6, N'Consultar', 2)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (7, N'CriarEditar', 3)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (8, N'Excluir', 3)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (9, N'Consultar', 3)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (10, N'CriarEditar', 1)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (11, N'Excluir', 1)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (12, N'Consultar', 1)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (13, N'CriarEditar', 4)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (14, N'Excluir', 4)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (15, N'Consultar', 4)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (16, N'CriarEditar', 5)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (17, N'Excluir', 5)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (18, N'Consultar', 5)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (19, N'CriarEditar', 6)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (20, N'Excluir', 6)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (21, N'Consultar', 6)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (22, N'CriarEditar', 7)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (23, N'Excluir', 7)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (24, N'Consultar', 7)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (25, N'CriarEditar', 8)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (26, N'Excluir', 8)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (27, N'Consultar', 8)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (28, N'Consultar', 10)
INSERT [dbo].[Acesso] ([AcessoId], [Descricao], [Menu]) VALUES (29, N'Consultar', 11)
SET IDENTITY_INSERT [dbo].[Acesso] OFF
GO
SET IDENTITY_INSERT [dbo].[PerfilXAcessos] ON 

INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (1, 1, 1)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (2, 1, 2)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (3, 1, 3)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (4, 1, 4)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (5, 1, 5)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (6, 1, 6)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (7, 1, 7)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (8, 1, 8)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (9, 1, 9)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (13, 1, 13)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (14, 1, 14)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (15, 1, 15)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (16, 1, 16)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (17, 1, 17)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (18, 1, 18)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (19, 1, 19)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (20, 1, 20)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (21, 1, 21)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (22, 1, 22)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (23, 1, 23)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (24, 1, 24)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (25, 1, 25)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (26, 1, 26)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (27, 1, 27)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (28, 1, 28)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (32, 2, 3)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (33, 2, 4)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (34, 2, 5)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (35, 2, 6)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (36, 2, 7)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (37, 2, 8)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (38, 2, 9)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (82, 2, 29)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (83, 2, 15)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (84, 2, 18)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (85, 2, 21)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (86, 2, 24)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (87, 2, 27)
INSERT [dbo].[PerfilXAcessos] ([Id], [PerfilId], [AcessoId]) VALUES (88, 1, 29)
SET IDENTITY_INSERT [dbo].[PerfilXAcessos] OFF
GO
SET IDENTITY_INSERT [dbo].[TipoStatus] ON 

INSERT [dbo].[TipoStatus] ([Id], [Descricao]) VALUES (1, N'Empresa')
INSERT [dbo].[TipoStatus] ([Id], [Descricao]) VALUES (2, N'Contrato')
INSERT [dbo].[TipoStatus] ([Id], [Descricao]) VALUES (3, N'Projeto')
SET IDENTITY_INSERT [dbo].[TipoStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (1, N'Na fila', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (3, N'Em Andamento', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (4, N'Em Homologação', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (5, N'Cancelado', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (6, N'Suspenso', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (7, N'Entregue', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (8, N'Implantado', 3)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (9, N'Ativo', 2)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (11, N'Bloqueado', 2)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (12, N'Cancelado', 2)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (13, N'Ativo', 1)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (14, N'Bloquado', 1)
INSERT [dbo].[Status] ([Id], [Descricao], [TipoStatusId]) VALUES (15, N'Implantados', 3)
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
SET IDENTITY_INSERT [dbo].[AuditoriaEvento] ON 

INSERT [dbo].[AuditoriaEvento] ([Id], [NomeEvento]) VALUES (1, N'Insercao')
INSERT [dbo].[AuditoriaEvento] ([Id], [NomeEvento]) VALUES (2, N'Alteracao')
INSERT [dbo].[AuditoriaEvento] ([Id], [NomeEvento]) VALUES (3, N'Exclusao')
INSERT [dbo].[AuditoriaEvento] ([Id], [NomeEvento]) VALUES (4, N'Logout')
INSERT [dbo].[AuditoriaEvento] ([Id], [NomeEvento]) VALUES (5, N'Login')
INSERT [dbo].[AuditoriaEvento] ([Id], [NomeEvento]) VALUES (6, N'Download')
SET IDENTITY_INSERT [dbo].[AuditoriaEvento] OFF
GO
SET IDENTITY_INSERT [dbo].[Categorizacao] ON 

INSERT [dbo].[Categorizacao] ([Id], [Descricao]) VALUES (1, N'Melhoria')
INSERT [dbo].[Categorizacao] ([Id], [Descricao]) VALUES (2, N'Novo Projeto')
INSERT [dbo].[Categorizacao] ([Id], [Descricao]) VALUES (3, N'Alteração de Regra')
INSERT [dbo].[Categorizacao] ([Id], [Descricao]) VALUES (4, N'Demanda Legal')
SET IDENTITY_INSERT [dbo].[Categorizacao] OFF
GO
SET IDENTITY_INSERT [dbo].[GrauUrgencia] ON 

INSERT [dbo].[GrauUrgencia] ([Id], [Descricao], [Ativo]) VALUES (1, N'Baixo', 1)
INSERT [dbo].[GrauUrgencia] ([Id], [Descricao], [Ativo]) VALUES (2, N'Medio', 1)
INSERT [dbo].[GrauUrgencia] ([Id], [Descricao], [Ativo]) VALUES (3, N'Alto', 1)
INSERT [dbo].[GrauUrgencia] ([Id], [Descricao], [Ativo]) VALUES (4, N'Legal', 1)
SET IDENTITY_INSERT [dbo].[GrauUrgencia] OFF
GO
INSERT [dbo].[Usuario] ([Funcional], [Nome], [Login], [Password], [Email], [Telefone], [Excluido], [Ativo], [PerfilAcessoId]) VALUES 
                      (N'admin', N'Administrador', N'admin', 'OUDZtpUict583Rdc2Oqnrg==', N'', N'', 0, 1, 1)
GO
