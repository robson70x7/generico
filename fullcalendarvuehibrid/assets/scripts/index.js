document.addEventListener('DOMContentLoaded', function () {
    new Vue({
        el: '#app',
        data: {
            title: 'Calendario'
        }
    });

    var calendEl = document.getElementById('calendario');
    const events = [
        {
            title: 'Cliente x', // a property!
            start: '2022-04-20T10:00', // a property!
            end: '2022-04-20T10:30',
            backgroundColor: '#ccc',
            display: 'backgroud',
            id:1,
            interactive: true,
        },
        {
            id:2,
            title: 'Reunião',
            start: '2022-04-21T10:00', // a property!
            end: '2022-04-21T10:30',
        },
    ];

    const dateClick = dateClickInfo => {
        const text = dateClickInfo.view.type == 'timeGridDay' ?
            `Evento para : ${dateClickInfo.dateStr}` : 'Evento para o dia inteiro';
        Swal.fire({
            title: 'Escreva o nome do evento',
            text,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Criar',
            showLoaderOnConfirm: true,
            backdrop:true,
            preConfirm: (event) => {
                if(!event) {
                    Swal.showValidationMessage('Evento não pode ser vazio');
                    return;
                }
                calendario.addEvent({
                    title: event,
                    start: dateClickInfo.date,
                    allDay:true,
                    id:calendario.getEvents().map(event => event.id).reduce((cur,acc) => {
                        if(acc < cur)
                            acc = cur;
                        
                        return acc;
                    }, 0)+1
                });
            
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }

    const select = dateClickInfo => {
        const text = dateClickInfo.view.type == 'timeGridDay' ?
            `Evento para o dia ${dateClickInfo.start.toLocaleDateString()} das ${dateClickInfo.start.toLocaleTimeString('pt-BR', { timeZone: 'UTC' })} - ${dateClickInfo.end.toLocaleTimeString('pt-BR', { timeZone: 'UTC' })}` :
            'Evento para o dia inteiro';
        Swal.fire({
            title: 'Escreva o nome do evento',
            text,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Criar',
            showLoaderOnConfirm: true,
            backdrop:true,
            preConfirm: (event) => {
                if(!event) {
                    Swal.showValidationMessage('Evento não pode ser vazio');
                    return;
                }
                calendario.addEvent({
                    title: event,
                    start: dateClickInfo.start,
                    end: dateClickInfo.end,
                    id:calendario.getEvents().map(event => event.id).reduce((cur,acc) => {
                        if(acc < cur)
                            acc = cur;
                        
                        return acc;
                    }, 0)+1
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }

    const drag = ({event,jsEvent}) => {
        var trashEl = document.getElementById('deleteEvent');

        var x1 = trashEl.offsetLeft;
        var x2 = trashEl.offsetLeft + 50;
        var y1 = trashEl.offsetTop;
        var y2 = trashEl.offsetTop + 50;

        if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 
          && jsEvent.pageY >= y1 && jsEvent.pageY <= y2
        ){
            Swal.fire({
                title: 'Atenção',
                text:'Tem certeza que deseja excluir este evento?',
                icon:'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                showConfirmButton:true,
                confirmButtonText: 'Excluir',
                backdrop:true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !Swal.isLoading()
            }).then(resp => {
                if(resp.isConfirmed){
                    let evento = calendario.getEventById(event.id)
                    evento.remove();
                    Swal.fire({
                        title:'Excluido',
                        text:'Evento excluido com sucesso',
                        icon:'success',
                        timer:1500
                    })
                }
            })
           
        }
    }

    const calendarOptions = {
        initialView: 'dayGridMonth',
        locale: 'pr-BR',
        themeSystem: 'bootstrap',
        timeZone: 'America/SaoPaulo',
        height: 650,
        navLinks: true,
        dayMaxEvents: true,
        buttonText: {
            today: 'hoje',
            month: 'mês',
            week: 'semana',
            day: 'dia',
            list: 'lista'
        },
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,dayGridWeek,timeGridDay'
        },
        events,
        nowIndicator: true,
        editable: true,
        selectable: true,
        dateClick,
        select,
        eventDragStop:drag
    }


    const calendario = new FullCalendar.Calendar(calendEl, calendarOptions);
    calendario.render();


    const btn = document.getElementById('fab-container');
    const options = btn.getElementsByClassName('options')[0];

    btn.addEventListener('mouseleave', function () {
        options.classList.remove('show');
        options.classList.add('hide');
    })
    btn.addEventListener('mouseover', function () {
        options.classList.add('show');
        options.classList.remove('hide');
    })


    function contextMenu() {
        var $contextMenu = $("#contextMenu");

        $("body").on("contextmenu", "table tr", function (e) {
            $contextMenu.css({
                display: "block",
                left: e.pageX,
                top: e.pageY
            });
            return false;
        });
    }
});
