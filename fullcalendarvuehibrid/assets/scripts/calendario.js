export default class CalenarioUtil{
    criarEventos(){
        const events = [];

        for(let i = 1; i <= 10; i++){
            let title = 'Minha tarefa '+i;
            let start = `2022-04-${i}T8:00:00`;
            let end = `2022-04-${i}T8:30:00`;
            
            events.push({
                title, // a property!
                start, // a property!
                end // a property! ** see important note below about 'end' **
            })
        }
    }
}
