USE [master]
GO
/****** Object:  Database [BD_CONTRATO]    Script Date: 17/01/2022 16:29:14 ******/
CREATE DATABASE [BD_CONTRATO];
GO
USE [BD_CONTRATO]
GO
/****** Object:  Table [dbo].[Acesso]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Acesso](
	[AcessoId] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [nvarchar](max) NULL,
	[Menu] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Acesso] PRIMARY KEY CLUSTERED 
(
	[AcessoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Aditivo]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Aditivo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumeroAditivo] [varchar](100) NOT NULL,
	[DataCriacao] [date] NOT NULL,
	[ContratoId] [int] NOT NULL,
	[NumeroContrato] [varchar](50) NOT NULL,
	[DataIniciao] [date] NOT NULL,
	[DataFim] [date] NOT NULL,
	[ValorTotal] [decimal](18, 2) NOT NULL,
	[QuantidadeHora] [int] NOT NULL,
	[ValorHora] [decimal](18, 2) NOT NULL,
	[StatusId] [int] NOT NULL,
	[EmpresaId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Anexo]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Anexo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Caminho] [varchar](500) NOT NULL,
	[Tamanho] [int] NOT NULL,
	[NomeArquivo] [varchar](255) NOT NULL,
	[ContratoId] [int] NULL,
	[ProjetoId] [int] NULL,
	[AditivoId] [int] NULL,
 CONSTRAINT [PK_Anexo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auditoria]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auditoria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mensagem] [nvarchar](max) NULL,
	[FuncionalUsuario] [char](7) NOT NULL,
	[DataHora] [datetime] NOT NULL,
	[IdAuditoriaEvento] [int] NOT NULL,
	[FuncionalidadeSistema] [varchar](100) NOT NULL,
 CONSTRAINT [PK_dbo.Auditoria] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuditoriaEvento]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditoriaEvento](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NomeEvento] [varchar](50) NULL,
 CONSTRAINT [PK_AuditoriaEvento] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categorizacao]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categorizacao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contrato]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contrato](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumeroContrato] [varchar](50) NOT NULL,
	[DataInicio] [date] NOT NULL,
	[DataFim] [date] NOT NULL,
	[ValorTotal] [decimal](18, 2) NOT NULL,
	[QuantidadeHora] [int] NOT NULL,
	[ValorHora] [decimal](18, 2) NOT NULL,
	[StatusId] [int] NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Ativo] [bit] NOT NULL,
 CONSTRAINT [PK__Contrato__3214EC0732222E81] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[CNPJ] [char](14) NOT NULL,
	[Logradouro] [varchar](255) NOT NULL,
	[Numero] [int] NOT NULL,
	[CEP] [char](8) NOT NULL,
	[Telefone] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[NomeRepresantante] [varchar](100) NOT NULL,
	[TelefoneRepresentante] [varchar](20) NULL,
	[EmailRepresentante] [varchar](100) NULL,
	[NumeroInscricaoEstadual] [varchar](50) NULL,
	[NumeroInscricaoMunicipal] [varchar](50) NULL,
	[StatusId] [int] NOT NULL,
	[Ativo] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Funcao]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Funcao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [varchar](100) NOT NULL,
	[Ativo] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GrauUrgencia]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GrauUrgencia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [varchar](100) NOT NULL,
	[Ativo] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogErro]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogErro](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mensagem] [varchar](255) NULL,
	[Data] [datetime] NOT NULL,
	[Exception] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PerfilXAcessos]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerfilXAcessos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PerfilId] [int] NOT NULL,
	[AcessoId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.PerfilXAcessos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Projeto]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projeto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DataSolicitacao] [date] NOT NULL,
	[GrauUrgenciaId] [int] NOT NULL,
	[SituacaoAtual] [text] NULL,
	[DescricaoSolicitacao] [text] NULL,
	[BeneficiosEsperado] [text] NULL,
	[ContratoId] [int] NOT NULL,
	[Solicitante] [varchar](100) NOT NULL,
	[AreaSolicitante] [varchar](100) NOT NULL,
	[GerenteSolicitante] [varchar](100) NULL,
	[GerenteDepartamentalSolicitante] [varchar](100) NULL,
	[QuantidadeHora] [int] NOT NULL,
	[DataInicio] [date] NOT NULL,
	[DataFim] [date] NOT NULL,
	[PrazoTotal] [int] NOT NULL,
	[ValorTotal] [decimal](18, 2) NOT NULL,
	[CategorizacaoId] [int] NOT NULL,
	[Observacao] [text] NULL,
	[StatusId] [int] NOT NULL,
	[NomePontoFocalEmpresa] [varchar](100) NULL,
	[TelefonePontoFocalEmpresa] [varchar](100) NULL,
	[EmailPontoFocalEmpresa] [varchar](100) NULL,
	[Ativo] [bit] NOT NULL,
	[HoraAlocacaoRecurso] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecursoContrato]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecursoContrato](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContratoId] [int] NOT NULL,
	[FuncaoId] [int] NOT NULL,
	[QuantidadeHora] [int] NOT NULL,
	[ValorHora] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecursoProjeto]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecursoProjeto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RecursoContratoId] [int] NOT NULL,
	[ProjetoId] [int] NOT NULL,
	[QuantidadeHora] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [varchar](100) NOT NULL,
	[TipoStatusId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoStatus]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 17/01/2022 16:29:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[Funcional] [char](7) NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [varchar](255) NULL,
	[Email] [varchar](100) NULL,
	[Telefone] [varchar](100) NULL,
	[Excluido] [bit] NOT NULL,
	[Ativo] [bit] NOT NULL,
	[PerfilAcessoId] [int] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Funcional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Aditivo] ADD  DEFAULT (getdate()) FOR [DataCriacao]
GO
ALTER TABLE [dbo].[Contrato] ADD  CONSTRAINT [DF_Contrato_Ativo]  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[Empresa] ADD  CONSTRAINT [DF_Empresa_Ativo]  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[Funcao] ADD  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[GrauUrgencia] ADD  CONSTRAINT [DF_GrauUrgencia_Ativo]  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[LogErro] ADD  DEFAULT (getdate()) FOR [Data]
GO
ALTER TABLE [dbo].[Projeto] ADD  CONSTRAINT [DF_Projeto_Ativo]  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_Excluido]  DEFAULT ((0)) FOR [Excluido]
GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_Ativo]  DEFAULT ((1)) FOR [Ativo]
GO
ALTER TABLE [dbo].[Aditivo]  WITH CHECK ADD  CONSTRAINT [FK_Aditivo_Contrato] FOREIGN KEY([ContratoId])
REFERENCES [dbo].[Contrato] ([Id])
GO
ALTER TABLE [dbo].[Aditivo] CHECK CONSTRAINT [FK_Aditivo_Contrato]
GO
ALTER TABLE [dbo].[Aditivo]  WITH CHECK ADD  CONSTRAINT [FK_Aditivo_Empresa] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresa] ([Id])
GO
ALTER TABLE [dbo].[Aditivo] CHECK CONSTRAINT [FK_Aditivo_Empresa]
GO
ALTER TABLE [dbo].[Aditivo]  WITH CHECK ADD  CONSTRAINT [FK_Aditivo_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Aditivo] CHECK CONSTRAINT [FK_Aditivo_Status]
GO
ALTER TABLE [dbo].[Anexo]  WITH CHECK ADD  CONSTRAINT [FK_Anexo_Aditivo] FOREIGN KEY([AditivoId])
REFERENCES [dbo].[Aditivo] ([Id])
GO
ALTER TABLE [dbo].[Anexo] CHECK CONSTRAINT [FK_Anexo_Aditivo]
GO
ALTER TABLE [dbo].[Anexo]  WITH CHECK ADD  CONSTRAINT [FK_Anexo_Contrato] FOREIGN KEY([ContratoId])
REFERENCES [dbo].[Contrato] ([Id])
GO
ALTER TABLE [dbo].[Anexo] CHECK CONSTRAINT [FK_Anexo_Contrato]
GO
ALTER TABLE [dbo].[Anexo]  WITH CHECK ADD  CONSTRAINT [FK_Anexo_Projeto] FOREIGN KEY([ProjetoId])
REFERENCES [dbo].[Projeto] ([Id])
GO
ALTER TABLE [dbo].[Anexo] CHECK CONSTRAINT [FK_Anexo_Projeto]
GO
ALTER TABLE [dbo].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_AuditoriaEvento] FOREIGN KEY([IdAuditoriaEvento])
REFERENCES [dbo].[AuditoriaEvento] ([Id])
GO
ALTER TABLE [dbo].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_AuditoriaEvento]
GO
ALTER TABLE [dbo].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Usuario] FOREIGN KEY([FuncionalUsuario])
REFERENCES [dbo].[Usuario] ([Funcional])
GO
ALTER TABLE [dbo].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Usuario]
GO
ALTER TABLE [dbo].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_Empresa] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresa] ([Id])
GO
ALTER TABLE [dbo].[Contrato] CHECK CONSTRAINT [FK_Contrato_Empresa]
GO
ALTER TABLE [dbo].[Contrato]  WITH CHECK ADD  CONSTRAINT [FK_Contrato_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Contrato] CHECK CONSTRAINT [FK_Contrato_Status]
GO
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_Empresa_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_Empresa_Status]
GO
ALTER TABLE [dbo].[PerfilXAcessos]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PerfilXAcessos_dbo.Acesso_AcessoId] FOREIGN KEY([AcessoId])
REFERENCES [dbo].[Acesso] ([AcessoId])
GO
ALTER TABLE [dbo].[PerfilXAcessos] CHECK CONSTRAINT [FK_dbo.PerfilXAcessos_dbo.Acesso_AcessoId]
GO
ALTER TABLE [dbo].[Projeto]  WITH CHECK ADD  CONSTRAINT [FK_Projeto_Categorizacao] FOREIGN KEY([CategorizacaoId])
REFERENCES [dbo].[Categorizacao] ([Id])
GO
ALTER TABLE [dbo].[Projeto] CHECK CONSTRAINT [FK_Projeto_Categorizacao]
GO
ALTER TABLE [dbo].[Projeto]  WITH CHECK ADD  CONSTRAINT [FK_Projeto_Contrato] FOREIGN KEY([ContratoId])
REFERENCES [dbo].[Contrato] ([Id])
GO
ALTER TABLE [dbo].[Projeto] CHECK CONSTRAINT [FK_Projeto_Contrato]
GO
ALTER TABLE [dbo].[Projeto]  WITH CHECK ADD  CONSTRAINT [FK_Projeto_GrauUrgencia] FOREIGN KEY([GrauUrgenciaId])
REFERENCES [dbo].[GrauUrgencia] ([Id])
GO
ALTER TABLE [dbo].[Projeto] CHECK CONSTRAINT [FK_Projeto_GrauUrgencia]
GO
ALTER TABLE [dbo].[Projeto]  WITH CHECK ADD  CONSTRAINT [FK_Projeto_Staus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([Id])
GO
ALTER TABLE [dbo].[Projeto] CHECK CONSTRAINT [FK_Projeto_Staus]
GO
ALTER TABLE [dbo].[RecursoContrato]  WITH CHECK ADD  CONSTRAINT [FK_RecursoContrato_Contrato] FOREIGN KEY([ContratoId])
REFERENCES [dbo].[Contrato] ([Id])
GO
ALTER TABLE [dbo].[RecursoContrato] CHECK CONSTRAINT [FK_RecursoContrato_Contrato]
GO
ALTER TABLE [dbo].[RecursoContrato]  WITH CHECK ADD  CONSTRAINT [FK_RecursoContrato_Funcao] FOREIGN KEY([FuncaoId])
REFERENCES [dbo].[Funcao] ([Id])
GO
ALTER TABLE [dbo].[RecursoContrato] CHECK CONSTRAINT [FK_RecursoContrato_Funcao]
GO
ALTER TABLE [dbo].[RecursoProjeto]  WITH CHECK ADD  CONSTRAINT [FK_RecursoProjeto_Projeto] FOREIGN KEY([ProjetoId])
REFERENCES [dbo].[Projeto] ([Id])
GO
ALTER TABLE [dbo].[RecursoProjeto] CHECK CONSTRAINT [FK_RecursoProjeto_Projeto]
GO
ALTER TABLE [dbo].[RecursoProjeto]  WITH CHECK ADD  CONSTRAINT [FK_RecursoProjeto_RecursoContrato] FOREIGN KEY([RecursoContratoId])
REFERENCES [dbo].[RecursoContrato] ([Id])
GO
ALTER TABLE [dbo].[RecursoProjeto] CHECK CONSTRAINT [FK_RecursoProjeto_RecursoContrato]
GO
ALTER TABLE [dbo].[Status]  WITH CHECK ADD  CONSTRAINT [FK_Status_Tipo] FOREIGN KEY([TipoStatusId])
REFERENCES [dbo].[TipoStatus] ([Id])
GO
ALTER TABLE [dbo].[Status] CHECK CONSTRAINT [FK_Status_Tipo]
GO
USE [master]
GO
ALTER DATABASE [BD_CONTRATO] SET  READ_WRITE 
GO
